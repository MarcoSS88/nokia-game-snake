import React from 'react';
import styled from 'styled-components'

import MainScreen from './components/main-screen/mainScreen.component';
import ScoreSection from './components/score-section/scoreSection.component';

const App = () => {
  return (
    <GlobalContainer>
      <MainScreen/>
      <ScoreSection/>
    </GlobalContainer>
  );
}

export default App;

const GlobalContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100vw;
  height: 100vh;
  background-color: #252525;
`;