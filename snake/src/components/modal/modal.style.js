import styled from "styled-components";

export const StartGameContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: auto;
  height: auto;
`;

export const Button = styled.button`
  width: 20rem;
  height: 4.5rem;
  font-family: "Press Start 2P", cursive;
  background-color: transparent;
  font-size: 1.8rem;
  color: #1f2f0d;
  border: 3px solid #1f2f0d;
  cursor: pointer;
`;

export const PlayCountdown = styled.p`
  font-size: 3.25rem;
  color: #1f2f0d;
`;
