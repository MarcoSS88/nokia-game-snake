import React, { useEffect } from "react";

import { StartGameContainer, Button, PlayCountdown } from "./modal.style";

const Modal = ({ countdown, setCountdown }) => {

  useEffect(() => {
    if (countdown < 4 && countdown > 0) {
      const interval = setInterval(() => {
        setCountdown(countdown - 1);
      }, 1000);
      return () => clearInterval(interval);
    }
  }, [countdown, setCountdown]);

  const playTheGame = () => setCountdown(countdown - 1);

  return (
    <StartGameContainer>
      {countdown === 4 && <Button onClick={() => playTheGame()}>Start Game</Button>}
      {countdown < 4 && countdown > 0 && <PlayCountdown>{countdown}</PlayCountdown>}
    </StartGameContainer>
  );
};

export default Modal;