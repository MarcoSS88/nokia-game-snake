import styled from 'styled-components';

export const Scheme = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 95%;
  height: 92%;
  border: 4px solid #1F2F0D;
`;