import React from 'react';

import { Counter } from './scoreCounter.style';

const ScoreCounter = ({ scoreType }) => <Counter> <p>{scoreType}</p> <p>0</p> </Counter> ;

export default ScoreCounter;