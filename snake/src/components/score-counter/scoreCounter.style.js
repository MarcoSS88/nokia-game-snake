import styled from 'styled-components';

export const Counter = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  width: auto;
  height: 100%;

  p {
    font-size: 1.35rem;
  }
`;