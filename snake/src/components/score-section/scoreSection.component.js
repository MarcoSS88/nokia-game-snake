import React, { useState } from "react";

import ScoreCounter from '../score-counter/scoreCounter.component';

import { Container } from "./scoreSection.style";

const ScoreSection = () => {

  const [scoreTable] = useState(["Score:", "apples:", "rabbits:"]);

  return (
    <Container>
      {scoreTable.map(scoreType => <ScoreCounter key={scoreType} scoreType={scoreType} />)}
    </Container>
  );
};

export default ScoreSection;