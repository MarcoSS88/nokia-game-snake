import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  width: 50rem;
  height: 4.5rem;
  border-radius: 0.5rem;
  background-color: #b4c101d2;
  padding: 0 2% 0 2%;
  margin-top: 2%;
`;