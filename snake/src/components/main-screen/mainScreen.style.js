import styled from 'styled-components';

export const Screen = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 50rem;
  height: 40rem;
  border-radius: 0.5rem;
  background-color: #b4c101d2;
`;