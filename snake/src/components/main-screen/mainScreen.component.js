import React, { useState } from 'react';

import { Screen } from './mainScreen.style';

import Modal from '../modal/modal.component';
import Game from '../game/game.component';

const MainScreen = () => {

    const [countdown, setCountdown] = useState(4);

    return (
        <Screen>
            {countdown !== 0 ? <Modal countdown={countdown} setCountdown={setCountdown}/> : <Game />}
        </Screen>
    );
}

export default MainScreen;